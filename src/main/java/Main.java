import io.grpc.Channel;
import sila2.org.silastandard.core.silaservice.v1.SiLAServiceGrpc;
import sila2.org.silastandard.core.silaservice.v1.SiLAServiceOuterClass;
import sila_java.library.core.sila.clients.ChannelFactory;
import sila_java.library.server_base.SiLAServer;
import sila_java.library.server_base.identification.ServerInformation;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Main {
    public static void main(String[] args) throws IOException {
        ServerInformation serverInformation = new ServerInformation("ExampleType",
                                                                    "Example description",
                                                                    "https://example.com",
                                                                    "0.1"
        );

        final Path keyFile = Paths.get(System.getProperty("user.home"), "key.pem");
        final Path certFile = Paths.get(System.getProperty("user.home"), "cert.pem");
        final int port = 50052;

        try (SiLAServer server = SiLAServer.Builder.withoutConfig(serverInformation)
                                                   .withPersistedTLS(keyFile, certFile,null)
                                                   .withPort(port)
                                                   .start()) {
            try (InputStream certStream = Files.newInputStream(certFile.toFile().toPath())) {
                Channel channel = ChannelFactory.getTLSEncryptedChannel("127.0.0.1", port, certStream);
                SiLAServiceGrpc.SiLAServiceBlockingStub stub = SiLAServiceGrpc.newBlockingStub(channel);
                String serverName = stub.getServerName(SiLAServiceOuterClass.Get_ServerName_Parameters.newBuilder().build()).getServerName().getValue();

                System.out.println("Server name is " + serverName);
            }
        }
    }
}
